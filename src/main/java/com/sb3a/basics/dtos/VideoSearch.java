package com.sb3a.basics.dtos;

public record VideoSearch(String name, String description) {
}
