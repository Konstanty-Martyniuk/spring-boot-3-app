package com.sb3a.basics.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class VideoEntity {

    private @Id @GeneratedValue Long id;
    private String name;
    private String description;

    public VideoEntity() {
        this(null, null);
    }

    public VideoEntity(String name, String description) {
        this.id = null;
        this.description = description;
        this.name = name;
    }
}
